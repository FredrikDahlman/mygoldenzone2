/*
 * User: joel
 * Date: 2013-02-23
 * Time: 19:22
 */
package com.caglabs.goldenzone.infomodel.dbloader;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.logging.Logger;

/**
 * Utils for DB
 */
public final class DBUtil {
    private static Logger logger = Logger.getLogger(DBUtil.class.getName());

    private DBUtil() {}

    public static void dropDB() {
        logger.info("Dropping the database");
        EntityManagerFactory ef = Persistence.createEntityManagerFactory("GoldenzonePU-dbloader-IT-Drop");
        EntityManager em = ef.createEntityManager();
        em.close();
        ef.close();
    }
}
